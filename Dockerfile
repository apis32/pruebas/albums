FROM openjdk:8u162-jdk-slim
COPY "./target/api-fotos-0.0.1-SNAPSHOT.jar" "app.jar"
ENTRYPOINT ["java", "-jar", "app.jar"]
