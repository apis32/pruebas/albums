package com.fotos.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fotos.api.entity.Album;
import com.fotos.api.entity.Foto;
import com.fotos.api.pojos.AlbumFull;
import com.fotos.api.pojos.Resultado;
import com.fotos.api.service.AlbumService;

@RestController
@RequestMapping("/album")
public class AlbumController {

	@Autowired
	private AlbumService albumService;
	
	@PostMapping
	public Album saveAlbum(@RequestBody Album album) {
		return albumService.saveAlbum(album);
	}

	@GetMapping(value = "/{idAlbum}")
	public AlbumFull capturarPokemon(@PathVariable int idAlbum) {
		AlbumFull albumFull = albumService.getAlbum(idAlbum);
		return albumFull;
	}
	
	@DeleteMapping(value = "/{idAlbum}")
	public Resultado deleteAlbum(@PathVariable int idAlbum) {
		return albumService.deleteAlbum(idAlbum);
	}

	@PostMapping(value = "/foto")
	public Foto saveFoto(@RequestBody Foto foto) {
		return albumService.saveFoto(foto);
	}
	
}
