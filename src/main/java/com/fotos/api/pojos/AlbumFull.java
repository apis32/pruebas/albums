package com.fotos.api.pojos;

import java.io.Serializable;
import java.util.List;

import com.fotos.api.entity.Album;
import com.fotos.api.entity.Foto;

public class AlbumFull implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8120869193380940770L;
	
	private String mensaje;
	private Album album;
	private List<Foto> fotos;
	public Album getAlbum() {
		return album;
	}
	public void setAlbum(Album album) {
		this.album = album;
	}
	public List<Foto> getFotos() {
		return fotos;
	}
	public void setFotos(List<Foto> fotos) {
		this.fotos = fotos;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
