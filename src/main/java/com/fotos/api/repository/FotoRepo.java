package com.fotos.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fotos.api.entity.Foto;

public interface FotoRepo extends JpaRepository<Foto, Integer> {
	public List<Foto> findByAlbumId(Integer albumId);
}
