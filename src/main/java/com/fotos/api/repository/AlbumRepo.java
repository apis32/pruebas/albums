package com.fotos.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fotos.api.entity.Album;

public interface AlbumRepo extends JpaRepository<Album, Integer> {

}
