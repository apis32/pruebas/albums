package com.fotos.api.service;

import com.fotos.api.entity.Album;
import com.fotos.api.entity.Foto;
import com.fotos.api.pojos.AlbumFull;
import com.fotos.api.pojos.Resultado;

public interface AlbumService {

	public Album saveAlbum(Album album);
	public AlbumFull getAlbum(int idAlbum);
	public Resultado deleteAlbum(int idAlbum);
	public Foto saveFoto(Foto foto);
	
}
