package com.fotos.api.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fotos.api.entity.Album;
import com.fotos.api.entity.Foto;
import com.fotos.api.pojos.AlbumFull;
import com.fotos.api.pojos.Resultado;
import com.fotos.api.repository.AlbumRepo;
import com.fotos.api.repository.FotoRepo;
import com.fotos.api.service.AlbumService;

@Service
public class AlbumServiceImpl implements AlbumService {

	@Autowired
	private AlbumRepo albumRepo;
	
	@Autowired
	private FotoRepo fotoRepo;
	
	@Override
	public Album saveAlbum(Album album) {
		album.setFechaCreacion(new Date());
		return albumRepo.save(album);
	}

	@Override
	public AlbumFull getAlbum(int idAlbum) {
		AlbumFull albumFull = new AlbumFull();
		String mensaje = "No se encontraron registros";
		Album album = null;
		List<Foto> lstFotos = null;

		try {
			album = albumRepo.findById(idAlbum).orElse(null);
			if(album != null) {
				lstFotos = fotoRepo.findByAlbumId(idAlbum);
				mensaje = "Registros encontrados";
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		albumFull.setAlbum( album );
		albumFull.setFotos( lstFotos );
		albumFull.setMensaje(mensaje);
		return albumFull;
	}

	@Override
	public Resultado deleteAlbum(int idAlbum) {
		Resultado resultado = new Resultado();
		boolean borradoCorrecto = false;
		String mensaje = "No existen registros para borrar";
		try {
			List<Foto> lst = fotoRepo.findByAlbumId(idAlbum);
			for(Foto f: lst) {
				fotoRepo.deleteById(f.getFotoId());
			}
			albumRepo.deleteById(idAlbum);
			borradoCorrecto = true;
			mensaje = "Album borrado correctamente";
		}catch(Exception e) {
			e.printStackTrace();
		}
		resultado.setResultado(borradoCorrecto);
		resultado.setMensaje(mensaje);
		return resultado;
	}

	@Override
	public Foto saveFoto(Foto foto) {
		try {
			Album album = albumRepo.findById(foto.getAlbumId()).orElse(null);
			if(album != null) {
				foto.setAlbumId(album.getAlbumId());
				foto.setFechaCreacion(new Date());
				foto = fotoRepo.save(foto);
			}else {
				foto.setAlbumId(-1);
				foto.setFotoId(-1);
				foto.setDescripcion("");
				foto.setUrl("");				
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return foto;
	}

}
