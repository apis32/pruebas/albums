# API Álbums

API que tiene por objetivo el alta, baja y borrado de álbums con sus fotografías, la base de datos utilizada es postgres.

## Requisitos previos
- Tener instalada la versión 8 del jdk
- Tener instalado el Spring Tool Suite 4
- Tener instalado Docker

Para la ejecución de la API se deben realizar los siguientes pasos:
1. Descargar proyecto
2. Abrir el Spring Tool Suite 4 e importar el proyecto
3. Abrir una consola, posicionarse en la carpeta del proyecto y generar el jar con el siguiente comando:
```cmd
mvn clean install -DskipTests=true
```
3. Si la construcción ha sido exitosa ejecutar el siguiente comando:
```cmd
docker-compose up
```
4. Una vez probado como se indica en la sección de Endpoints ejecutar el siguiente comando para dar de baja los servicios de la API y de la base de datos.
```cmd
docker-compose down
```

## Endpoints

Esta API está compuesta por los siguientes endpoints:

### 1. Crear álbum
POST -- localhost:8080/album

**Body entrada:**
```json
{
    "albumId": "",
    "nombre": "album dos",
    "fechaCreacion": null
}
```
**Salidas:** objeto completo creado
```json
{
    "albumId": 2,
    "nombre": "album dos",
    "fechaCreacion": "2022-10-09T05:57:45.921+00:00"
}
```

### 2. Crear foto
POST -- localhost:8080/album/foto

**Body entrada:**
```json
{
    "fotoId": "",
    "descripcion": "foto dos",
    "url": "https://www.baeldung.com/foto12.jpg",
    "fechaCreacion": null,
    "albumId": 2
}
```
**Salidas:** objeto completo creado, en caso de error, la respuesta es un objeto con atributo `fotoId` -1, en caso de que no exista el `albumId` indicado en el objeto de entrada
```json
{
    "fotoId": 2,
    "descripcion": "foto dos",
    "url": "https://www.baeldung.com/foto12.jpg",
    "fechaCreacion": "2022-10-09T05:58:09.634+00:00",
    "albumId": 2
}
```

### 3. Consultar album con sus fotos
GET -- localhost:8080/album/{`idAlbum` a buscar, valor int}

**Salida:** en caso de error el atributo mensaje tendra una leyenda de "No se encontraron registros" y los objetos album y fotos tendrán valor null
```json
{
    "mensaje": "Registros encontrados",
    "album": {
        "albumId": 2,
        "nombre": "album dos",
        "fechaCreacion": "2022-10-09"
    },
    "fotos": [
        {
            "fotoId": 2,
            "descripcion": "foto dos",
            "url": "https://www.baeldung.com/foto12.jpg",
            "fechaCreacion": "2022-10-09",
            "albumId": 2
        }
    ]
}
```

### 4. Borrar album con sus fotos
DELETE -- localhost:8080/album/{`idAlbum` a buscar, valor int}

**Salidas:**
```json
{
    "resultado": true,
    "mensaje": "Album borrado correctamente"
}
{
    "resultado": false,
    "mensaje": "No existen registros para borrar"
}
```

**Notas** 
- Esta versión no requiere autenticación
